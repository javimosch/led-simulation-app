

## Installation

    - npm install
    
## Development

    - npm run dev
    - edit css in src/public/css/main.scss (SASS)
    - edit javascript in src/public/js 
    - edit html in src/public/views
    
## Wordpress deploy

    - You need to copy paste an entire html string
    - run: **npm run start** to build the production files
    - Copy the follow folders to httpdocs: dist, img, fonts
    - Paste the follow html (raw) in a Page section. (see embed-code.txt)
    
    
## Heroku deploy

    - deploy to heroku and will run automatically