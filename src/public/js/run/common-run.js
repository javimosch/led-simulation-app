(function() {
    /*global $U*/
    /*global angular*/
    /*global $*/
    /*global moment*/
    /*global _*/
    /*global localStorageDB*/
    /*global localStorage*/
    /*global $hasMouse*/
    var APP_NAME = "LEDSIM-APP"
    var STORE_SESSION_PREFIX = "-CACHE";
    angular.module('common-run', [])
        .config(['$logProvider', function($logProvider) {
            var enabled = window.location.hostname.indexOf('c9users.io') !== -1 || window.location.hostname.indexOf('localhost') !== -1 || window.location.hostname.indexOf('herokuapp') !== -1 || window.location.hostname.indexOf('62.210.97.81');
            //console.info('$log is ', (enabled ? 'enabled' : 'disabled'));
            $logProvider.debugEnabled(enabled);
        }]).run(['$timeout', '$rootScope', function($timeout, $rootScope) {
            $U.exposeGlobal('r', $rootScope);
            $rootScope.momentFormat = (d, f) => (moment(d).format(f));
            $rootScope.momentTime = (d) => moment(d).format('HH[h]mm');
            $rootScope.momentFrenchDateTime = (d) => moment(d).format('DD/MM/YYYY [à] HH[h]mm');
            $rootScope.momentDateTime = (d) => $rootScope.momentFrenchDateTime(d); // moment(d).format('DD-MM-YY HH[h]mm');
            $rootScope.momentDateTimeWords = (d, article) => moment(d).format('[' + (article || 'Le') + '] dddd DD MMMM YY [à] HH[h]mm');
            $rootScope.momentDateTimeWords2 = (d) => moment(d).format('dddd DD MMMM YY [à] HH[h]mm');
            $rootScope.dom = function(cb, timeout) {
                $timeout(function() {
                    if (cb) {
                        cb();
                    }
                    $rootScope.$apply();
                }, timeout || 0);
            };
            $rootScope.toggleBody = function(val) {
                $rootScope.dom(function() {
                    var el = document.body;
                    el.className = el.className.replace('hidden', '').trim();
                    if (!val) {
                        el.className = (el.className + ' hidden').trim();
                    }
                });
            };

            function createHash(str) {
                return APP_NAME + window.btoa(window.location.hostname) + window.btoa(str || '1');
            }

            $rootScope.session = function(data) {
                var id = createHash('S');
                if (data) {
                    $U.store.set(id, data);
                    $rootScope._session = data;
                }
                $rootScope._session = $U.store.get(id) || {};
                if (!$rootScope._session) {
                    $U.store.set(id, {});
                    $rootScope._session = {};
                }
                return $rootScope._session;
            };
            $rootScope.sessionMetadata = function(data, reset) {
                var id = createHash('M');
                reset = (reset != undefined) ? reset : false;
                if (data) {
                    if (reset) {
                        $U.store.set(id, data);
                        return data || {};
                    }
                    else {
                        var combinedData = $U.store.get(id) || {};
                        Object.assign(combinedData, data);
                        $U.store.set(id, combinedData);
                        return combinedData || {};
                    }
                }
                else {
                    return $U.store.get(id) || {};
                }
            };
            $rootScope.session();

            //
            setTimeout(function() {
                $rootScope.$apply(function() {
                    $('.remove-hidden').removeClass('hidden').removeClass('remove-hidden');
                });
            }, 300);

        }]);

})();
