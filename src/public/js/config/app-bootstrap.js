(function() {
    /*global angular*/
    /*global moment*/
    angular.module('app', [
        'common-run',
        //'focusOn',
        //'pretty-checkable',
        //'ngSanitize',
    ]);
    moment.locale('fr');
})();
