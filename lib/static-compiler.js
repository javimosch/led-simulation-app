"use strict";
var path = require('path');
var Promise = require("promise");
var logger = require(path.join(process.cwd(),'lib/logger'));
var farmhash = require('farmhash');
var CleanCSS = require('clean-css');
var minifyHTML = require('html-minifier').minify;
let co = require("co");
var sgUtilsParser = require('./utils/html-parser-util');
var sgUtils = require('./utils/common-utils');
var sgScript = require('./js-compiler');
var hbs = require('handlebars');
var readDirFiles = require('read-dir-files');
var mkdirp = require('mkdirp');
var fs = require('fs');
var sgData = require('./config/env');
var COMMON_PATH = process.cwd() + '/src/partials';
var SRC_STATIC_FOLDER = process.cwd() + '/src/static';
var SRC_PARTIALS_FOLDER = process.cwd() + '/src/partials';
const OUTPUT_PATH = path.join(process.cwd(), '/src/public')
var PARTIALS_EXT = 'html';
var global_partials = null;
var env = require(path.join(process.cwd(),'lib/config/env'));
var PROD = env().PROD;
var removeHtmlComments = require('remove-html-comments');
const RELATIVE_PATH_FOR_VENDOR_CUSTOM = 'public';
var ENABLE_MINIFY_JS = (process.env.MINIFYJS !== undefined) ? process.env.MINIFYJS.toString() == '1' : true;
var ENABLE_SOURCEMAPS = (process.env.SOURCEMAPS !== undefined) ? process.env.SOURCEMAPS.toString() == '1' : false;
function getOutput(str) {
    return path.join(OUTPUT_PATH, str);
}
var _watches = {};



var _events = {};

module.exports = {
    pathPartials: (p) => SRC_PARTIALS_FOLDER = p,
    pathStatic: (p) => SRC_STATIC_FOLDER = p,
    dest: (dest) => {
        //DIST = dest;
    },
    watch: () => {
        watchPartials();
        watchSrc();
    },
    build: build,
    on: (evt, handler) => {
        _events[evt] = _events[evt] || [];
        _events[evt].push(handler);
    }
}

function build() {
    return new Promise((resolve, error) => {
        co(function*() {
            //yield buildTemplatesPartials(COMMON_PATH);
            yield buildTemplatesPartials();
            yield buildTemplates();
            resolve(true);
        }).catch(function(err) {
            console.log(err);
        });

    });
}

function emit(evt, p) {
    _events[evt] = _events[evt] || [];
    _events[evt].forEach(handler => handler(p));
}

function watchSrc() {
    //console.log('DEBUG: Waching Static Folder: ' + SRC);
    sgUtils.watch(SRC_STATIC_FOLDER, () => {
        buildTemplates();
    });
}

function watchPartials() {
    //console.log('DEBUG: Waching Partials Folder: ' + PATH);
    sgUtils.watch(SRC_PARTIALS_FOLDER, build);
}

function* buildTemplatesPartials(src, append) {
    return new Promise(function(resolve, err) {
        co(function*() {
            if (!fs.existsSync(src || SRC_PARTIALS_FOLDER)) return console.log('DEBUG:  tpl partials skip for', src || SRC_PARTIALS_FOLDER);
            global_partials = sgUtils.normalizeFilesTreePreservePath(readDirFiles.readSync(src || SRC_PARTIALS_FOLDER));
            global_partials = sgUtils.filesIncludeOnly(global_partials, PARTIALS_EXT);
            let keys = Object.keys(global_partials),
                key = null,
                name, remoteData, isAfter = false;
            for (var x in keys) {
                key = keys[x];
                name = key.substring(key.lastIndexOf('/') + 1);
                name = name.substring(0, name.lastIndexOf('.') !== -1 && name.lastIndexOf('.') || undefined);
                let fullPath = path.join(src || path.join(process.cwd(), SRC_PARTIALS_FOLDER), key);
                //Fetch the file data from firebase (if any)
                //remoteData = yield heFirebase.getPartialContent(fullPath);
                //If file modified date is after firebase file date, we use the file content. If not, we use the firebase file.
                //if(remoteData){
                //    isAfter = yield heUtils.fileIsAfterDate(fullPath, remoteData.updatedAt);    
                // }else{
                isAfter = false;
                //    console.log('WARN: Firebase data not found',name);
                // }

                // if (isAfter) {
                //    hbs.registerPartial(name, sgUtils.urldecode(remoteData.content)); //We finally //register the handlebar partial
                //    console.log('TEMPLATES: Using firebase version for ', name);
                // }
                // else {
                //If we plan to use the local file data, we send the file to firebase

                hbs.registerPartial(name, global_partials[key]); //We finally register the handlebar partial
                // }
                //console.log('COMPILE-DEBUG: Partial registered', name);
            }
            //console.log('DEBUG: partials',global_partials);
            resolve(true);
        }).catch(function(err) {
            console.log('WARN: Templates Partials compile error', err);
        });
    });
}





var vendorData = {};

function vendorChanges(path, arr) {
    if (!vendorData[path]) {
        vendorData[path] = arr;
        return true;
    }
    else {
        if (arr.length !== vendorData[path].length) {
            vendorData[path] = arr;
            return true;
        }
        else {

            for (var x in vendorData[path]) {
                if (vendorData[path][x] !== arr[x]) {
                    vendorData[path] = arr;
                    return true;
                }
            }
            return false;
        }
    }
}


function buildTemplates() {

    logger.debugTerminal('Handlebar data keys', Object.keys(sgData()));

    return new Promise((resolve, error) => {


        if (global_partials == null) {
            //not ready yet;
            console.error('DEBUG: build static no partials yet');
            process.exit(1);
            return;
        }

        //console.log('TEMPLATES:BUILD:STATIC', heConfig().output());


        //console.log('ss debug templates building to ['+heConfig().output()+']');



        function needsCompilation(raw, path) {
            return raw.indexOf('HBSIGNORE') == -1;
        }

        function handleNewFileTransform(raw, path) {
            return new Promise((resolve, reject) => {
                var rta = raw;
                if (needsCompilation(raw, path)) {

                    //console.log('COMPILE-DEBUG', 'handleNewFileTransform start');
                    try {
                        var rta = hbs.compile(raw)(sgData());
                        //console.log('COMPILE-DEBUG', 'handleNewFileTransform hbs passed');
                        if (PROD) {
                            rta = minifyHTML(rta, {
                                removeAttributeQuotes: false,
                                removeScriptTypeAttributes: true,
                                collapseWhitespace: true,
                                minifyCSS: true,
                                minifyJS: true,
                                caseSensitive: true
                            });
                            //console.log('COMPILE-DEBUG', 'handleNewFileTransform bundling passed');
                        }
                        return resolve(rta);
                    }
                    catch (_err) {
                        console.log('COMPILE-ERROR', _err);
                    }
                }
                else {
                    return resolve(raw);
                }
            });
        }

        //console.log('COMPILE-DEBUG: Copy from ', SRC_STATIC_FOLDER, 'to', OUTPUT_PATH);
        sgUtils.copyFilesFromTo(SRC_STATIC_FOLDER, OUTPUT_PATH, {
            formatPathHandler: (path) => {
                if (path.indexOf('index') !== -1) {
                    path = path.substring(0, path.lastIndexOf('index')) + 'index.html';
                }
                return path;
            },
            formatContentHandler: (raw, path, _resolve) => {
                //console.log('COMPILE-DEBUG: formatContentHandler start', path);
                var rta = raw;
                handleNewFileTransform(raw, path).then((rta) => {
                    logger.debugTerminal('Handling',path);
                    _resolve(rta);
                }).catch(err => {
                    logger.errorTerminal(err);
                });
                //console.log('COMPILE-DEBUG: formatContentHandler end', path);
            }
        }).then((res) => {
            if(res.ok){
                logger.debugTerminal("Finish");
            }else{
                logger.warnTerminal("Finish with errors");
            }
            emit('build-success');
            resolve();
        }).catch(logger.error);
    });

}
